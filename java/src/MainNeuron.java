import java.util.Arrays;
import java.util.List;

public class MainNeuron {

    public static void bindInput(double[] data, Neuron[] firstLayer){
        for(int i=0 ; i < firstLayer.length ; i++){ 
            firstLayer[i].setInput(data);
        }
    }

    public static void bindLayers(Neuron[] prec, Neuron[] next){
        for(int i=0 ; i < prec.length; i++){
            prec[i].setNextNodes(Arrays.asList(next));
        }
        for(int i=0 ; i < next.length; i++){
            next[i].setPrevNodes(Arrays.asList(prec));
        }
    }

    public static void main(String[] args) {
        double[] output = {0,0,0} ;
        double[] exceptedOutput = { 0,1,1 } ;
        double[][] dataset = { {3,3,0}
                             , {1,1,1}
                             , {0,3,2} 
                             } ;

        Neuron[] firstLayer = 
            { new Neuron(3) 
            , new Neuron(3) 
            , new Neuron(3)
            } ;
        Neuron[] secondLayer =
            { new Neuron(3) 
            , new Neuron(3)
            , new Neuron(3) 
            } ;
        Neuron[] thirdLayer =
            { new Neuron(3) 
            , new Neuron(3)
            , new Neuron(3) 
            } ;
        Neuron[] layer4 = 
            { new Neuron(3) } ;

        bindLayers(firstLayer,secondLayer);
        bindLayers(secondLayer,thirdLayer);
        bindLayers(thirdLayer,layer4);
        int i = 0;
        while(i < 10000 ){
            bindInput(dataset[i%3], firstLayer);
            for(int j=0; j < firstLayer.length; j++) 
                firstLayer[j].backtracking(exceptedOutput[i%3]);
            output[i%3] = layer4[0].output();
            i++;
        };
        System.out.println("Expected output vs actual output");
        for(int k=0; k < output.length; k++){ 
           System.out.println(exceptedOutput[k%3]+" "+output[k%3]);
        }
    }


}
