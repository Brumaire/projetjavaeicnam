
import java.util.Random;

public class Perceptron {
///////////////////////////////////////////////////////////////////////////
// Object zone
///////////////////////////////////////////////////////////////////////////

    public static final double BIAS = 0.1;

    /**
     * Vector of weights - w
     **/
    protected double[] weights;

    public double[] getWeights() {
        return this.weights;
    }

    /**
     * Constructor taking features number parameter and performing weights initialisazation.
     **/
    public Perceptron(int featuresNb) {
        Random rdm = new Random();
        this.weights = new double[featuresNb];
        for( int i= 0; i < featuresNb; i++ ) {
           this.weights[i] = rdm.nextDouble(); 
        }
    }

    /**
     * Activation function.
     **/
    protected double activationFunction(double x) {
        return x <= 0 ? 0
                      : 1 ;
    }

    /**
     * Method that compute the output of the pereptron
     **/
    protected double outputFunction(double[] xVector) {
        double accumulator = 0;

        //Compute the scalar product
        for( int i= 0; i < this.weights.length; i++ ) {
            accumulator += xVector[i] * this.weights[i];
        }

        //Perform the activation operation and return
        return this.activationFunction(accumulator);

    }

    /**
     * Method that performs the weights modification
     * w ⟵  w+α( y2-h(x2) ) x2
     **/
    public void backtracking(double[] xVector, double expectedOutput, double output) {
        for( int i= 0; i < this.weights.length; i++ ) {
            this.weights[i] = this.weights[i] + Perceptron.BIAS * ( expectedOutput - output ) 
                                                                * xVector[i]; 
        }
    }

    /**
     * Method that perform a learning step for a given value 
     *
     * returns false if results are not matching and a backtracking is performed, true otherwise
     **/
    public boolean learningStep(double[] xVector, double expectedOutput) {
        boolean result = true;
        double output = outputFunction(xVector);
        if(output != expectedOutput) {
            this.backtracking(xVector, expectedOutput, output);
            result = false;
        }
        return result;
    }

    /**
     * Method for making prediction on a data sample.
     **/
    public double predict(double[] xVector) {
        return outputFunction(xVector);
    }
}
