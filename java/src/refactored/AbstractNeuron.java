import java.util.List;
import java.util.ArrayList;
import java.lang.Math;
import java.util.Random;


public abstract class AbstractNeuron {

///////////////////////////////////////////////////////////////////////////////
// Object Zone
///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Attributes, getters and setters
    ///////////////////////////////////////////////////////////////////////////
        
        /** Bias for the weight adjustement computation **/
        private final double BIAS ;    

        /** The weight associated with each entry **/
        private List<Double> weights;

        public double       getBias()         { return this.BIAS;              } 
        public List<Double> getWeights()      { return this.weights;           } 

        public List<AbstractNeuron> getPrevNodes() { return null; }
        public List<AbstractNeuron> getNextNodes() { return null; }

        public void setPrevNodes(List<AbstractNeuron> prev) { }
        public void setNextNodes(List<AbstractNeuron> next) { }

    ///////////////////////////////////////////////////////////////////////////
    // Constructors
    ///////////////////////////////////////////////////////////////////////////

        /**
         * Constructor that define the bias(alpha) value, and the number of
         * expected entry (featureNB)
         **/
        public AbstractNeuron(double bias, int featuresNb) {
            Random rdm = new Random();
            
            this.BIAS = bias;
            this.weights = new ArrayList<Double>();
            for ( int i= 0; i < featuresNb; i++ ) {
                this.weights.add( new Double(rdm.nextDouble()) ) ;
            }
        }

    ///////////////////////////////////////////////////////////////////////////
    // Public Methods
    ///////////////////////////////////////////////////////////////////////////
    
        /** Method that performs the backpropagation operation
         *
         * Error computation and spreading and weight adjustement.
         * @param expectedOutput the expected output for a traning sample.
         * @return the error for a given neuron.
         **/ 
        public abstract double backpropagation(double expectedOutput);

        /** Method that compute the neuron activity 
         *
         * Implementation SHALL :
         * 1- get the entry 
         * 2- perform scalar product 
         * 3- execute activation function
         * 4- return the result
         **/
        public abstract double outputFunction();

        public List<AbstractNeuron> toList(){
            List<AbstractNeuron> list = new ArrayList<AbstractNeuron>();
            list.add(this);
            return list;
        }

    ///////////////////////////////////////////////////////////////////////////
    // Private and protected Methods
    ///////////////////////////////////////////////////////////////////////////

        /** Method that compute the result of the activation function **/
        protected double activationFunction(double x) {
            return 1 / ( 1 + Math.exp(-x) );
        }

        /** Method that compute the w*x scalar product **/
        protected double scalarProduct(double[] xVector ) {
            double accumulator = 0;
            for( int i= 0; i < this.weights.size(); i++ ) {
                accumulator += xVector[i] * this.weights.get(i);
            }
            return accumulator;
        }
}
