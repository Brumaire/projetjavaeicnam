import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

/** A neuron in the output layer of the network */
public class OutputNeuron extends AbstractNeuron {
///////////////////////////////////////////////////////////////////////////////
// Object Zone
///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Attributes, getters and setters
    ///////////////////////////////////////////////////////////////////////////

        /** The list of G(n-1) neurons **/
        private List<AbstractNeuron> prevNodes;
        
        @Override
        public List<AbstractNeuron> getPrevNodes() { return this.prevNodes; }
        @Override
        public void setPrevNodes(List<AbstractNeuron> prev) { this.prevNodes = prev; }

    ///////////////////////////////////////////////////////////////////////////
    // Attributes, getters and setters
    ///////////////////////////////////////////////////////////////////////////
    
        public OutputNeuron(double bias, int featureNb) {
            super(bias, featureNb);
        }

    ///////////////////////////////////////////////////////////////////////////
    // Implementation
    ///////////////////////////////////////////////////////////////////////////

    public double backpropagation (double expectedOutput) {
        double nw;
        double result = this.outputFunction();
        double error = expectedOutput - result;
        List<Double> weights = this.getWeights();
        for( int i=0 ; i < weights.size() ; i++ ) {
            nw = weights.get(i) + ( this.getBias() 
                                    * this.getPrevNodes().get(i).outputFunction()
                                    * error 
                                  ) ;
            weights.set(i,nw); 
        }
        return error;
    }
    public double outputFunction() {
        double[] xEntry = new double[this.getPrevNodes().size()];
        for(int i=0 ; i < xEntry.length; i++) {
            xEntry[i] = this.getPrevNodes().get(i).outputFunction();
        }
        return this.activationFunction(this.scalarProduct(xEntry));
    }
}
