import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

/**
 * A Neuron in the hidden layers of the network.
 **/
public class InterNeuron extends AbstractNeuron {

///////////////////////////////////////////////////////////////////////////////
// Object Zone
///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Attributes, getters and setters
    ///////////////////////////////////////////////////////////////////////////
        
        /** The list of G(n-1) neurons **/
        private List<AbstractNeuron> prevNodes;
        /** the list of G(n+1) neurons **/
        private List<AbstractNeuron> nextNodes;
        
        @Override
        public List<AbstractNeuron> getPrevNodes() { return this.prevNodes; }
        @Override
        public List<AbstractNeuron> getNextNodes() { return this.nextNodes; }
        @Override 
        public void setPrevNodes(List<AbstractNeuron> prev) { this.prevNodes = prev; }
        @Override
        public void setNextNodes(List<AbstractNeuron> next) { this.nextNodes = next; }

    ///////////////////////////////////////////////////////////////////////////
    // Constructors
    ///////////////////////////////////////////////////////////////////////////
    
        public InterNeuron(double bias, int featureNb) {
            super(bias, featureNb);
        }

    ///////////////////////////////////////////////////////////////////////////
    // Private and protected methods
    ///////////////////////////////////////////////////////////////////////////

        private Double getAssociatedWeight(AbstractNeuron n) {
            List<AbstractNeuron> neurons = n.getPrevNodes();
            int i = neurons.indexOf(this);
            return n.getWeights().get(i);
        }

    ///////////////////////////////////////////////////////////////////////////
    // Implementations
    ///////////////////////////////////////////////////////////////////////////
    
    public double backpropagation(double expectedOutput) {
        double nw;
        double error;
        double result = this.outputFunction();
        double accumulator = 0;
        List<Double> weights = this.getWeights();

        for(AbstractNeuron n : this.getNextNodes() ) {
            accumulator += this.getAssociatedWeight(n) * n.backpropagation(expectedOutput);
        }
        error = result * (1 - result) * accumulator;

        for( int i=0 ; i < weights.size() ; i++ ) {
            nw = weights.get(i) + ( this.getBias() 
                                    * this.getPrevNodes().get(i).outputFunction()
                                    * error 
                                  ) ;
            weights.set(i,nw); 
        }
        return error;
    }

    public double outputFunction() {
        double[] xEntry = new double[this.getPrevNodes().size()];
        for(int i=0 ; i < xEntry.length; i++) {
            xEntry[i] = this.getPrevNodes().get(i).outputFunction();
        }
        return this.activationFunction(this.scalarProduct(xEntry));
    }

}
