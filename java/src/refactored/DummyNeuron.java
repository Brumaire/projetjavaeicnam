import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

public class DummyNeuron extends AbstractNeuron {
/**
 * An almost empty AbstractNeuron implementation the main purpose in providing
 * a starting point for the backpropagation recursive implementation.
 **/

///////////////////////////////////////////////////////////////////////////////
// Object Zone
///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Attributes, getters and setters
    ///////////////////////////////////////////////////////////////////////////
 
        /**
         * The list of entry neurons for a given neural network
         **/
        private List<AbstractNeuron> inputNeurons;

        @Override
        public void setNextNodes(List<AbstractNeuron> next) { this.inputNeurons = next; }

    ///////////////////////////////////////////////////////////////////////////
    // Constructor
    ///////////////////////////////////////////////////////////////////////////
    
        public DummyNeuron(double bias, int featureNb) {
            super(bias, featureNb);
        }


    ///////////////////////////////////////////////////////////////////////////
    // Methods Implementation
    ///////////////////////////////////////////////////////////////////////////
        public double outputFunction() {
            return -1;
        }

        public double backpropagation(double expectedOutput){
            for( AbstractNeuron n : this.inputNeurons ) {
                n.backpropagation(expectedOutput);
            }
            return -1;
        }
}
