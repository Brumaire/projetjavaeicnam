
# À Propos

## Auteurs 
Auteurs : Lucas Vincent, Baptiste Catois
Contact : lucas.vincent3@protonmail.com

## Description

Le matériel est une preuve de concept des réseaux de neurones pour la
classification linéaire, implémentée en Java et réalisée dans le cadre d'un
module d'enseignement de l'EICNAM Grand-Est.

## Versions

Le programme a été écrit avec java 14 (openjdk) mais est pleinement compatible
avec une version de java supérieure à la 8.

## Utilisation

Se référer à la javadoc de la classe NeuralNetwork.

Une petite démonstration est fournie via la classe NeuralNetworkTest.
