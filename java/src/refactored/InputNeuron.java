import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

/**
 * A neuron in the input layer of the network
 */
public class InputNeuron extends AbstractNeuron {
///////////////////////////////////////////////////////////////////////////////
// Object Zone
///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    // Attributes, getters and setters
    ///////////////////////////////////////////////////////////////////////////
    
        /** The input values for neurons of the entry layer. **/
        private double[] input;

        /** List of G(n+1) Neurons **/
        private List<AbstractNeuron> nextNodes;
        
        @Override
        public void setNextNodes(List<AbstractNeuron> next) { this.nextNodes = next; }
        @Override
        public List<AbstractNeuron> getNextNodes() { return this.nextNodes; }

        public void setInput(double[] input) { this.input = input; };

    ///////////////////////////////////////////////////////////////////////////
    // Constructors
    ///////////////////////////////////////////////////////////////////////////
    
        public InputNeuron(double bias, int featureNb) {
            super(bias, featureNb);
        }

    ///////////////////////////////////////////////////////////////////////////
    // Private and protected methods
    ///////////////////////////////////////////////////////////////////////////
    
        private Double getAssociatedWeight(AbstractNeuron n) {
            List<AbstractNeuron> neurons = n.getPrevNodes();
            int i = neurons.indexOf(this);
            return n.getWeights().get(i);
        }

    ///////////////////////////////////////////////////////////////////////////
    // Methods Implementation
    ///////////////////////////////////////////////////////////////////////////
 
        public double backpropagation(double expectedOutput) {
            double nw;
            double error;
            double result = this.outputFunction();
            double accumulator = 0;
            List<Double> weights = this.getWeights();

            for(AbstractNeuron n : this.getNextNodes() ) {
                accumulator += this.getAssociatedWeight(n) * n.backpropagation(expectedOutput);
            }
            error = result * (1 - result) * accumulator;

            for( int i=0 ; i < weights.size() ; i++ ) {
                nw = weights.get(i) + ( this.getBias() 
                                    * this.input[i]
                                    * error 
                                  ) ;
                weights.set(i,nw); 
            }
            return error;
            }

        public double outputFunction(){
            double[] input = this.input ;
            return activationFunction ( this.scalarProduct(input) ) ;
        }

}
