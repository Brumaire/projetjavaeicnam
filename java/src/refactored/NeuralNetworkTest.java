import java.util.Arrays;
import java.lang.Math;

public class NeuralNetworkTest {
    
    // Le jeu de données, avec les valeurs attendues à la fin de chaque
    // sous-tableau.
    public final static double[][] dataset = 
    { { 3,3,0, 0 }
    , { 1,1,1, 1 }
    , { 0,3,2, 1 }
    } ;

    public static void main(String[] args) {
        double[] result;
        // Bias = 0,1, featuresNb = 3, Inner layers = 2
        NeuralNetwork nn = new NeuralNetwork(0.1,3,2);
        // 1000 learning step over the dataset
        nn.train(1000, dataset);
        // Make a predict on the given dataset
        result = nn.predictDataset(dataset);

        //Rint the result, as the purpose of the network is linear
        //classification
        for(int d=0 ; d < result.length ; d++) {
            result[d] = Math.rint(result[d]);
        }
        //Show the dataset
        System.out.println("Training dataset with class annotation :");
        for(double[] d : dataset) {
            System.out.println(Arrays.toString(d));
        }
        //Show the result.
        System.out.println("Inference result on previous dataset :");
        System.out.println(Arrays.toString(result));

        //Test
        for(int i=0; i < result.length; i++) {
            assert result[i] == dataset[i][3];
        }
    }
}
