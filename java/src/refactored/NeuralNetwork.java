import java.util.List;
import java.util.ArrayList;
import java.lang.Math;


public class NeuralNetwork {

    ///////////////////////////////////////////////////////////////////////////
    // Attributes, getters and setters
    ///////////////////////////////////////////////////////////////////////////

        protected DummyNeuron dummy;
        protected List <AbstractNeuron> inputLayer;
        protected List <List<AbstractNeuron>> innerLayers;
        protected OutputNeuron outputNeuron;

    ///////////////////////////////////////////////////////////////////////////
    // Protected Methods
    ///////////////////////////////////////////////////////////////////////////
        /**
         * Method that bind 2 layers of the network
         * The given implementation connect all neurons in l1 to all neurons
         * in l2.
         **/
        protected void bindLayers(List<AbstractNeuron>layer1, List<AbstractNeuron> layer2) {
            for(AbstractNeuron n : layer1) {
                n.setNextNodes( layer2 );
            }
            for(AbstractNeuron n : layer2) {
                n.setPrevNodes( layer1 );
            }
        }

        /**
         * Method that bind all the layers of the network
         **/
        protected void bindLayers(){
            this.bindLayers(this.dummy.toList(),this.inputLayer);
            this.bindLayers(this.inputLayer,this.innerLayers.get(0));
            for (int i=1 ; i < this.innerLayers.size() ; i++) {
                this.bindLayers(this.innerLayers.get(i-1), this.innerLayers.get(i));
            }
            this.bindLayers(this.innerLayers.get(this.innerLayers.size()-1),outputNeuron.toList());
        }


    ///////////////////////////////////////////////////////////////////////////
    // Constructors
    ///////////////////////////////////////////////////////////////////////////

        /**
         * Constructor that builds a neural network with a givent bias,
         * featureNb and layers number.
         * The number of neurons per layer equals the nb of entry features.
         **/
        public NeuralNetwork(double bias, int featuresNb, int nbLayers){
            if ( featuresNb <= 0 || nbLayers < 0 || bias < 0 ) {
                System.err.println("Errors in provided network parameters.");
                System.exit(1);
            }
            else {
                this.innerLayers = new ArrayList<List<AbstractNeuron>>();
                this.inputLayer = new ArrayList<AbstractNeuron>();
                for(int k=0; k < featuresNb; k++) {
                    this.inputLayer.add(new InputNeuron(bias, featuresNb));
                }
                for(int j=0 ; j < nbLayers ; j++) {
                    List<AbstractNeuron> layer = new ArrayList<AbstractNeuron>();
                    this.innerLayers.add(layer);
                    for(int i=0 ; i < featuresNb ; i++) {
                        layer.add(new InterNeuron(bias, featuresNb));
                    }   
                }
                this.dummy = new DummyNeuron(bias, featuresNb);
                this.outputNeuron = new OutputNeuron(bias,featuresNb);
                this.bindLayers();
            }   
        }
            
    ///////////////////////////////////////////////////////////////////////////
    // FeaturesNb
    ///////////////////////////////////////////////////////////////////////////
        
        /**
         * This method trains the networks with a given number of steps
         * and a given training set.
         *
         * The last element in each sample array SHALL be the expected output
         **/
        public void train(int stepNumber, double[][] trainingDataset) {
            double[] sample;
            for(int i=0 ; i < stepNumber; i++ ) {
                for(int j=0; j < trainingDataset.length ; j++) {
                    sample = trainingDataset[j];
                    for(AbstractNeuron n : this.inputLayer) {
                        InputNeuron nb = (InputNeuron) n;
                        nb.setInput(sample);
                    }
                    dummy.backpropagation(sample[sample.length-1]); 
                }
            }
        }

        /**
         * Do an inference on a sample
         **/
        public double predict(double[] sample) {
            for(AbstractNeuron n : this.inputLayer) {
                InputNeuron nb = (InputNeuron) n;
                nb.setInput(sample);
            }
            return outputNeuron.outputFunction();
        }

        /**
         * Do an inference on each samble off a dataset
         * @return an array with the result
         **/
        public double[] predictDataset(double[][] dataset) {
            double[] result = new double[dataset.length];
            for( int i=0 ; i < dataset.length ; i++ ) {
                result[i] = predict(dataset[i]);
            }
            return result;
        }
}
