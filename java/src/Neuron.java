import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

public class Neuron extends Perceptron {

    private List<Neuron> prevNodes;
    private List<Neuron> nextNodes;
    
    //getters and setteers
    public List<Neuron> getPrevNodes() { return this.prevNodes; }
    public void setPrevNodes(List<Neuron> prev) { this.prevNodes = prev; }
    public List<Neuron> getNextNodes() { return this.nextNodes;}
    public void setNextNodes(List<Neuron> next) { this.nextNodes = next; }

    private double[] input;
    //getters and setters
    public double[] getInput() { return this.input; }
    public void setInput(double[] inputs) { this.input = inputs; }

    public Neuron(int featureNb, Neuron[] prec, Neuron[] next) {
        super(featureNb);
        if( prec == null ) this.prevNodes = null ;
        else {
            this.prevNodes = new ArrayList<Neuron>();
            this.populate(prec,prevNodes);
        }
        if( next == null) this.nextNodes = null;
        else {
            this.nextNodes = new ArrayList<Neuron>();
            this.populate(next,nextNodes);
        }
    }

    public Neuron(int featureNb) {
        this(featureNb, null, null);
    }

    /**
     * Peupler les lists de neurones
     **/
    private void populate(Neuron[] layer, List<Neuron> synapses) {
        for( int i= 0; i < layer.length; i++ ) {
            synapses.add(layer[i]);
        }
    }

    /**
     * Récupérer outputs de la couche précédente
     **/
    public double[] getPrevLayerVector() {
        double[] result = null;
        if(this.getPrevNodes() == null || this.input != null) {
            result = this.getInput();
        }
        else {
            result = new double[this.prevNodes.size()];
            for( int i=0; i < result.length; i++ ) {
                result[i] = this.prevNodes.get(i).outputFunction(
                        this.prevNodes.get(i).getPrevLayerVector());
            }
        }
        return result;
    }

    public double output() {
        return this.outputFunction(getPrevLayerVector());
    }


    /**
     * Backtracking algorithm
     **/
    public double backtracking(double expectedOutput) {
        double error; 
        double[] prevVector = this.getPrevLayerVector();
        double result = 0;
        result = outputFunction(prevVector);    
        // cas de récursion terminal.
        if(this.nextNodes == null) {
          error = expectedOutput - result;
        }
        else {
          double accumulator = 0;
          for(Neuron n : this.getNextNodes() ){
              accumulator += this.getAssociatedWeight(n) * n.backtracking(expectedOutput);
          }
          error = result * ( 1 - result) * accumulator;
        }
        for(int i=0; i < weights.length; i++){
            weights[i] = weights[i] + Neuron.BIAS * prevVector[i] * error;
        }
        return error;
    }

    private double getAssociatedWeight(Neuron n) {
        List<Neuron> neurons = n.getPrevNodes();
        int i = neurons.indexOf(this);
        return n.getWeights()[i];
    }
    @Override 
    /**
     * Redéfinition de la fonction d'activation
     **/
    protected double activationFunction(double x) {
        return 1 / ( 1 + Math.exp(-x) ) ;
    }
    
}
