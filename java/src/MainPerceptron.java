
import java.util.Arrays;

public class Main {
    private static double[][] DataSet = {
            {1, 2, 0},
            {1, 0, 3},
            {1, 3, 0},
            {1, 1, 1}
    };

    private static final int[] BREAKPOINT = {1, 1, 1, 1};
    private static int[] EXPECTED_OUTPUTS = {1, 0, 0, 1};

    public static void main(String[] args) {
        Perceptron perceptron = new Perceptron(3);

        int[] bufferBacktracking = {0, 0, 0, 0};

        //Executed while there is no backtracking
        while(!Arrays.equals(bufferBacktracking, BREAKPOINT)) {
            for(int i = 0; i < DataSet.length; i++) {
                bufferBacktracking[i] = perceptron.learningStep(DataSet[i], EXPECTED_OUTPUTS[i]) ? 1 : 0;
                System.out.printf("Vector[%d] : " + Arrays.toString(DataSet[i])
                        + "\n | Perceptron Return : " + perceptron.predict(DataSet[i])
                        + "\n | Expected return   : %d \n" , i, EXPECTED_OUTPUTS[i]);
            }

            System.out.println("Weights in Perceptron : " + Arrays.toString(perceptron.getWeights()));
        }
    }
}
